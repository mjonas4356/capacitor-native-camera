package cz.mjonas.nativecamera.capacitornativecamera;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Calendar;

import com.getcapacitor.JSObject;
import com.getcapacitor.NativePlugin;
import com.getcapacitor.Plugin;
import com.getcapacitor.PluginCall;
import com.getcapacitor.PluginMethod;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Log;

import cz.mjonas.nativecamera.capacitornativecamera.NatCameraActivity;

@NativePlugin(
    requestCodes={NativeCamera.REQUEST_CAPTURE}
)

public class NativeCamera extends Plugin {

    protected static final int REQUEST_CAPTURE = 12345;
    private static final String TAG = "NativeCamera";

    private Uri imageUri;
    private File photo;
    private static final String _DATA = "_data";
    private String date = null;

    @PluginMethod
    public void echo(PluginCall call) {
        String value = call.getString("value");
        android.util.Log.d(TAG, "NCP: echo started");

        JSObject ret = new JSObject();
        ret.put("value", value);
        call.success(ret);
    }

    @PluginMethod
    public void capture(PluginCall call) {
        android.util.Log.d(TAG, "NCP: capture started");
        Integer quality = call.getInt("quality");
        saveCall(call);

        Intent intent = new Intent(getActivity().getApplicationContext(), NatCameraActivity.class);
        android.util.Log.d(TAG, "NCP: capture - after new intentent");

        this.photo = createCaptureFile();
        this.imageUri = Uri.fromFile(photo);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, this.imageUri);

        Log.d(TAG, "imageUri ---> " + this.imageUri);

        startActivityForResult(call, intent, REQUEST_CAPTURE);
    }

    private File createCaptureFile() {
        Calendar c = Calendar.getInstance();
        this.date = "" + c.get(Calendar.DAY_OF_MONTH)
                + c.get(Calendar.MONTH)
                + c.get(Calendar.YEAR)
                + c.get(Calendar.HOUR_OF_DAY)
                + c.get(Calendar.MINUTE)
                + c.get(Calendar.SECOND);

        return new File(getTempDirectoryPath(getContext()), "pic-" + this.date + ".jpg");
    }

    public void handleOnActivityResult(int requestCode, int resultCode, Intent data) {

        Log.d(TAG, "handleOnActivityResult");

        super.handleOnActivityResult(requestCode, resultCode, data);

        PluginCall savedCall = getSavedCall();

        if (savedCall == null) {
            return;
        }

        if (resultCode == Activity.RESULT_OK) {
            String thumbnail = this.photo.getAbsolutePath().replace(".jpg", "-th.jpg");
            makeThumbnail(this.photo.getAbsolutePath(), thumbnail);
            JSObject ret = new JSObject();
            String img = this.imageUri.toString();

            Log.d(TAG, "Image captured " + img);
            ret.put("image", img);
            ret.put("thumbnail", thumbnail);
            savedCall.resolve(ret);
        } else if (resultCode == Activity.RESULT_CANCELED) {
            Log.d(TAG, "Camera cancelled.");
            savedCall.reject("Camera cancelled.");
        } else {
            Log.d(TAG, "Camera error.");
            savedCall.reject("Camera error.");
        }
    }

    public void makeThumbnail(String src, String dst) {
        Log.d(TAG, ">>> makethumbnail - src : " + src);
        Log.d(TAG, ">>> makethumbnail - dst : " + dst);
        Bitmap thumbImage = ThumbnailUtils.extractThumbnail(BitmapFactory.decodeFile(src), 140, 210);
        FileOutputStream fts = null;
        try {
            fts = new FileOutputStream(dst);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        thumbImage.compress(Bitmap.CompressFormat.JPEG, 50, fts);
        try {
            fts.flush();
            fts.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        thumbImage.recycle();
    }

    private String getTempDirectoryPath(Context ctx) {
        File dir = null;

        dir = ctx.getExternalFilesDir(null);
        if (!dir.exists()) {
            dir.mkdirs();
        }

        return dir.getAbsolutePath();
    }

}
