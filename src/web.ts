import { WebPlugin } from '@capacitor/core';
import { NativeCameraPlugin } from './definitions';

export class NativeCameraWeb extends WebPlugin implements NativeCameraPlugin {
  constructor() {
    super({
      name: 'NativeCamera',
      platforms: ['web'],
    });
  }

  async echo(options: { value: string }): Promise<{ value: string }> {
    console.log('ECHO', options);
    return options;
  }

  async capture(options: { quality: number }): Promise<{ image: string, thumbnail: string }> {
    console.log('CAPTURE', options);
    return { image: '', thumbnail: '' };
  }

}

const NativeCamera = new NativeCameraWeb();

export { NativeCamera };

import { registerWebPlugin } from '@capacitor/core';
registerWebPlugin(NativeCamera);
