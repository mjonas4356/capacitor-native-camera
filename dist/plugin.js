var NativeCamera = (function (exports, core) {
    'use strict';

    var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
        function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
        return new (P || (P = Promise))(function (resolve, reject) {
            function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
            function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
            function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
            step((generator = generator.apply(thisArg, _arguments || [])).next());
        });
    };
    class NativeCameraWeb extends core.WebPlugin {
        constructor() {
            super({
                name: 'NativeCamera',
                platforms: ['web'],
            });
        }
        echo(options) {
            return __awaiter(this, void 0, void 0, function* () {
                console.log('ECHO', options);
                return options;
            });
        }
        capture(options) {
            return __awaiter(this, void 0, void 0, function* () {
                console.log('CAPTURE', options);
                return { image: '', thumbnail: '' };
            });
        }
    }
    const NativeCamera = new NativeCameraWeb();
    core.registerWebPlugin(NativeCamera);

    exports.NativeCamera = NativeCamera;
    exports.NativeCameraWeb = NativeCameraWeb;

    Object.defineProperty(exports, '__esModule', { value: true });

    return exports;

}({}, capacitorExports));
//# sourceMappingURL=plugin.js.map
