import { WebPlugin } from '@capacitor/core';
import { NativeCameraPlugin } from './definitions';
export declare class NativeCameraWeb extends WebPlugin implements NativeCameraPlugin {
    constructor();
    echo(options: {
        value: string;
    }): Promise<{
        value: string;
    }>;
    capture(options: {
        quality: number;
    }): Promise<{
        image: string;
        thumbnail: string;
    }>;
}
declare const NativeCamera: NativeCameraWeb;
export { NativeCamera };
