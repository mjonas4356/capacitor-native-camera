declare module '@capacitor/core' {
    interface PluginRegistry {
        NativeCamera: NativeCameraPlugin;
    }
}
export interface NativeCameraPlugin {
    echo(options: {
        value: string;
    }): Promise<{
        value: string;
    }>;
    capture(options: {
        quality: number;
    }): Promise<{
        image: string;
        thumbnail: string;
    }>;
}
